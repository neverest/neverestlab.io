#!/usr/bin/env python

import argparse
import json
import os


def genereate_inventory():
    return {
        "neverest": {
            "hosts": [
                os.getenv("SSH_HOST"),
            ],
            "vars": {
                "ansible_ssh_user": os.getenv("SSH_USER"),
                "ansible_ssh_pass": os.getenv("SSH_PASS"),
            },
        }
    }


def empty_inventory():
    return {"_meta": {"hostvars": {}}}


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--list", action="store_true")
    parser.add_argument("--host", action="store")
    args = parser.parse_args()

    if args.list:
        print(json.dumps(genereate_inventory(), indent=6))
    else:
        print(json.dumps(empty_inventory(), indent=6))
