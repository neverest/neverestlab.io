---
title: "Kontakt"
date: 2017-10-04T20:47:56+02:00
draft: false
weitgh: 40
---

### deepsearch26@gmail

<form method="POST" action="http://formspree.io/deepsearch26@gmail.com">
	<div class="field half first">
		<label for="name">Meno</label>
		<input type="text" name="name" id="name" />
	</div>
	<div class="field half">
		<label for="email">Email</label>
		<input type="text" name="_replyto" id="email" />
	</div>
	<div class="field">
		<label for="message">Správa</label>
		<textarea name="email" id="email" rows="4"></textarea>
	</div>
	<ul class="actions">
		<li><input type="submit" value="Odoslať" class="special" /></li>
		<li><input type="reset" value="Reset" /></li>
	</ul>
</form>

{{< socialLinks >}}
