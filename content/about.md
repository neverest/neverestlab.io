﻿---
title: "O projekte"
date: 2017-10-04T20:47:56+02:00
draft: false
weight: 1
---

Informačná explózia so sebou prináša aj viacero problémov. Napriek tomu, že v dnešnej dobe si nemožno sťažovať na nedostatok informácií, máme často problém nájsť to, čo práve potrebujeme. Väčšina dokumentov je totiž v neštrukturovanej podobe a  získať z nich informácie typu: kto v danom období pôsobil v určitom regióne je prakticky nemožné. Fulltextové vyhľadávanie má jeden vážny nedostatok a to, že nezohľadňuje sémantiku daných kľúčových slov.

V našom projekte sa zameriavame na spracovanie prirodzeného jazyka a stanovili sme si pomerne ambiciózny cieľ, ktorým je extrakcia štruktúrovaných dát z neštruktúrovaného textu so zachytením ich významu. Pričom zameriavať sa budeme najmä na životopisy a iné dokumenty, z ktorých budeme môcť extrahovať informácie o tom kde a kedy dané osoby študovali, prípadne pôsobili. Pokúsime sa tiež z textov získať vzťahy typu: kolega alebo spolužiak.

Našou úlohou bude teda rozpoznávať, a pokiaľ to bude možné aj jednoznačne identifikovať, entity typu osoba, korporácia, geograficke lokacie, datácia a zároveň identifikovať udalosť štúdium, prípadne pôsobenie a v rámci nich vzťahy medzi týmito entitami prepojiť.

Cieľom je tieto údaje uložiť v štruktúrovanej podobe tak, aby bolo možné v nich vyhľadávať a získať informáciu o tom kto a kedy v danom mieste študoval, s kým sa mohol poznať a vytvárať tak aj virtuálne komunity napr. pre určité zameranie.

---

# Ciele projektu

### 1. Semester

Cieľom tohto semestra je vytvorenie databázy na základe neštruktúrovaných životopisov. Je potrebné identifikovať nasledovné polia:

	• Narodenie – dátum a miesto

	• Úmrtie – dátum a miesto

	• Štúdium – dátum, miesto, profesori, spolužiaci

V ideálnom prípade by bolo vhodné taktiež vizualizovať súvislosti medzi osobnosťami pomocou grafu. Jednotlivé vrcholy budú predstavovať osobnosti a hrany budú predstavovať súvislosti medzi nimi.

### 2. Semester

Ciele pre tento semester sú:

	• Integrácia neo4j databázy
	
	• Opraviť a vylepšiť vyhľadávanie entít

	• Vizualizovania dát pomocou webovej aplikácie


	


