﻿---
title: "Progres"
date: 2017-10-04T20:47:56+02:00
draft: false
weight: 1


---
### 1.5.2018

Aplikácia je stabilne nasadená na serveri. Dolaďujú sa posledné chyby na front-ende aj back-ende. Momentálne sa venujeme finálnej dokumentácii diela.

---
### 23.4.2018

Úspešne sme sa zúčastnili TP Cup súťaže tímových projektov na IITSRC 2018. Podarilo sa nám odprezentovať náš produkt viacerým hodnotiacim a iným záujemcom. Na výsledky stále čakáme.

---
### 12.4.2018

V poslednom období sa venujeme príprave na IITSRC 18.4.2018. Aplikácia je v rámci možností prezentovateľná. 

---
### 26.3.2018

V rámci back-endu sa nám podarilo opäť odstrániť ďalšie chyby, ktoré vznikali kvôli anomáliám v textoch.

---
### 9.3.2018

Podarilo sa nám spojazdniť databázu Neo4j aj na serveri.

---
### 2.3.2018

Pri hľadaní vhodného zaradenia pre nášho nového kolegu v tíme sme sa rozhodli mu zveriť front-end. V priebehu prvého šprintu sa nám podarilo rozbehať základný koncept webovej aplikácie. Taktiež sa nám podarilo sfinalizovať článok na IITSRC, ktorého rozšírený abstrakt si môžete pozrieť v sekcii "TP CUP".

---
### 24.2.2018

Do tímu nám pribudol nový člen. Pri tejto príležitosti sme taktiež začali pracovať na článku pre TP Cup, ktorého sa zúčastníme v rámci IIT.SRC 2018 v apríli 2018.

---
### 15.2.2018

Podarilo sa nám navrhnúť prvý databázový model a vložiť aktuálne spracované dáta do databázy Neo4j. Momentálne pracujeme s lokálnymi verziami databáz. Podarilo sa nám odstrániť niektoré chyby pri rozpoznávaní entít a ich vzťahov na základe lepšieho predspracovania samotných textov.

---
### 14.12.2017

Počas prvého semestra sme sa ako tím museli vysporiadať s odchodom dvoch členov. Aj napriek tejto skutočnosti sa nám pomerne úspešne podarilo spojazdniť back-end softvéru. Identifikácia entít a vzťahov v predbežnej evaluácii dosahuje okolo 50% úspešnosť. Sú potrebné úpravy a zmeny.

---



	


