﻿---
title: "Členovia"
date: 2017-10-04T20:47:56+02:00
draft: false
weight: 2
---

---
<img style="float: left; margin-right: 15px; height: 80px;" src="images/team/peter.jpg">

#### Berta Peter, Bc.
Scrum Master

*Peter je študentom inžinierskeho štúdia na fakulte informatiky a informačných technológií v Bratislave. Počas bakalárskeho štúdia sa venoval výskumu identifikácie prípadov použitia v zdrojovom kóde. Má rozsiahle skúsenosti s programovacími jazykmi C, Java, Python a Javascript. V rámci svojho diplomového projektu sa bude zaoberať organizačnými vzormi a vzorovými jazykmi. V tíme má pridelenú rolu scrum mastera.*

---

<img style="float: left; margin-right: 15px; height: 80px;" src="images/team/matej.jpg">

#### Adamov Matej, Bc.
Master Director of Team, Communication and Customer, Interaction

*Matej momentálne po dokončení bakalárskeho stupňa na fakulte informatiky a informačných technológii v odbore informatika, na tejto fakulte pokračuje aj na inžinierskom stupni v odbore inteligentné softvérové systémy. V tíme sa venuje manažmentu komunikácie a kontroly. Medzi Matejove obľúbené oblasti patrí strojové učenie, objavovanie znalostí a proces návrhu systémov.*

---

<img style="float: left; margin-right: 15px; height: 80px;" src="images/team/brona.png">

#### Pečíková Bronislava, Bc.
Database expert

*Bronislava vyštudovala bakalárske štúdium v odbore aplikovaná matematika a aktuálne študuje inžinierske štúdium na fakulte informatiky a informačných technológii v odbore inteligentné softvérové systémy, tento odbor si obľúbila a to najmä preto, že ju baví dátová analýza, objavovanie znalostí a strojové učenie. V tíme zastrešuje prácu s dátami, teda výber databázy návrh štruktúry dát optimalizáciu dopytov okrem toho je hlavným zodpovedným za dokumentáciu a to nie len oficiálnu projektovú dokumentáciu ale aj dokumentáciu riadenia a rôzne “pracovné” dokumentácie nevyhnutné pre dobré fungovanie vývoja. No a v neposlednom rade je jej úlohou aj prezentácia tímu prostredníctvom webovej stránky, za ktorej obsah zodpovedá.*

---

<img style="float: left; margin-right: 15px; height: 80px;" src="images/team/unknown.png">

#### Krempaský Michal, Bc.
Lead Testing Coordinator

*Michal vyštudoval bakalárske štúdium na fakulte informatika a informačných technológií v odbore informatika a pokračuje ďalej na inžinierskom stupni v odbore inteligentní softvérové systémy. Jeho úloha v tíme je manažment rizík, kde sa snaží identifikovať potencionálne rizika, ktorí by mohli negatívne ovplyvniť projekt. Okrem rizík má na starosti aj tému testovania, kde vie prispieť aj skúsenosťami z praxe. Jeho obľubené témy v informatike sú ohľadom umelej inteligencie a modelovanie. Rád hľadá chyby v systémoch a riešenia pri vzniknutých problémoch.*

---

<img style="float: left; margin-right: 15px; height: 80px;" src="images/team/unknown.png">

#### Ondrej Hamara, Bc.
Developer

*Ondrej vyštudoval bakalárske štúdium na fakulte informatiky a informačných technológií v odbore informatika a pokračuje ďalej na inžinierskom stupni v odbore softvérové inžinierstvo. Jeho úlohou v tíme je správa webovej stránky tímu a vývoj frontendovej applikácie na webe. Okrem toho na starosti má aj vizualizáciu dát. Jeho obľubené oblasti v IT svete sú modelovanie systémov pomocou UML a ich zobrazovanie v 3D priestore. Nepohrdne ani prácou na backhande pri vývoji logiky systémov v OO programovaní.*

---