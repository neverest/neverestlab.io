﻿---
title: "TP CUP"
date: 2017-10-04T20:47:56+02:00
draft: false
weight: 4
---

### Title: Building Ontologies from Unstructured Biographies

---

### Extended Abstract:

Great amount of information is stored in historical books. 
However, majority of this knowledge is hidden within long texts. 
At this moment, the extracted information only describes main concepts, for example date of birth or death. 
Lots of data describing connections between people or communities remain undiscovered. 
Nowadays, with accessibility of digitalized texts, many opportunities for automating discovery of these connections arise.
 
In this paper, we focus on building ontologies from unstructured biographies using named entity recognition. 
Result is visualized as graph of entities and connections between them.  
Using this tool, one can easily search through processed biographies and review potential interesting information.