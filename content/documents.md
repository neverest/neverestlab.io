﻿---
title: "Dokumenty"
date: 2017-10-04T20:47:56+02:00
draft: false
weight: 4
---

### Zápisnice zo stretnutí - zimný semester

Popis					| Dátum				| 																			|
------------------------|-------------------|---------------------------------------------------------------------------|
Zapisnica 1     		|September 25, 2017	|<a href="documents/TP_zapisnica1.pdf" download>Stiahnuť</a>				|		
Zapisnica 2				|Október 2, 2017	|<a href="documents/TP_zapisnica2.pdf" download>Stiahnuť</a>				|
Zapisnica 3				|Október 9, 2017	|<a href="documents/TP_zapisnica3.pdf" download>Stiahnuť</a>				|
Zapisnica 4				|Október 16, 2017	|<a href="documents/TP_zapisnica4.pdf" download>Stiahnuť</a>				|
Zapisnica 5				|Október 23, 2017	|<a href="documents/TP_zapisnica5.pdf" download>Stiahnuť</a>				|
Zapisnica 6				|Október 30, 2017	|<a href="documents/TP_zapisnica6.pdf" download>Stiahnuť</a>				|
Zapisnica 7				|November 6, 2017	|<a href="documents/TP_zapisnica7.pdf" download>Stiahnuť</a>				|
Zapisnica 8				|November 13, 2017	|<a href="documents/TP_zapisnica8.pdf" download>Stiahnuť</a>				|
Zapisnica 9				|November 20, 2017	|<a href="documents/TP_zapisnica9.pdf" download>Stiahnuť</a>				|
Zapisnica 10			|November 28, 2017	|<a href="documents/TP_zapisnica10.pdf" download>Stiahnuť</a>				|
Zapisnica 11			|December 8, 2017	|<a href="documents/TP_zapisnica11.pdf" download>Stiahnuť</a>				|
Zapisnica 12			|December 11, 2017	|<a href="documents/TP_zapisnica12.pdf" download>Stiahnuť</a>				|

### Zápisnice zo stretnutí - letný semester

Popis					| Dátum				| 																			|
------------------------|-------------------|---------------------------------------------------------------------------|
Zapisnica 1     		|Február 13, 2018	|<a href="documents/TP_zapisnica_ls_01.pdf" download>Stiahnuť</a>				|		
Zapisnica 2     		|Február 20, 2018	|<a href="documents/TP_zapisnica_ls_02.pdf" download>Stiahnuť</a>				|	
Zapisnica 3     		|Február 27, 2018	|<a href="documents/TP_zapisnica_ls_03.pdf" download>Stiahnuť</a>				|
Zapisnica - Medzistretnutie     		|Marec 6, 2018	|<a href="documents/TP_zapisnica_ls_medz1.pdf" download>Stiahnuť</a>				|
Zapisnica 4     		|Marec 9, 2018	|<a href="documents/TP_zapisnica_ls_04.pdf" download>Stiahnuť</a>	
Zapisnica 5     		|Marec 13, 2018	|<a href="documents/TP_zapisnica_ls_05.pdf" download>Stiahnuť</a>			|
Zapisnica 6     		|Apríl 3, 2018	|<a href="documents/TP_zapisnica_ls_06.pdf" download>Stiahnuť</a>			|
Zapisnica 7     		|Apríl 10, 2018	|<a href="documents/TP_zapisnica_ls_07.pdf" download>Stiahnuť</a>			|
Zapisnica 8     		|Apríl 17, 2018	|<a href="documents/TP_zapisnica_ls_08.pdf" download>Stiahnuť</a>			|
Zapisnica 9     		|Apríl 24, 2018	|<a href="documents/TP_zapisnica_ls_09.pdf" download>Stiahnuť</a>			|

### Retrospektíva

Popis					| Dátum				| 																			|
------------------------|-------------------|---------------------------------------------------------------------------|
Retrospektíva     		|Máj 11, 2018	|<a href="documents/Retrospektiva.pdf" download>Stiahnuť</a>				|

### Zimný semester - 1. Kontrolný bod

Popis					| Dátum				| 																			|
------------------------|-------------------|---------------------------------------------------------------------------|
Dokumentácia riadenia	|November 17, 2017	|<a href="documents/DokumentaciaRiadenia_01.pdf" download>Stiahnuť</a>		|		
Projektová dokumentácia	|November 17, 2017	|<a href="documents/ProjektovaDokumentacia_01.pdf" download>Stiahnuť</a>	|
Metodiky				|November 17, 2017	|<a href="documents/Metodiky_01.pdf" download>Stiahnuť</a>					|
Export evidencie úloh	|November 17, 2017	|<a href="documents/ExportEvidencieUloh.pdf" download>Stiahnuť</a>			|


### Zimný semester - 2. Kontrolný bod

Popis					| Dátum				| 																			|
------------------------|-------------------|---------------------------------------------------------------------------|
Dokumentácia riadenia	|December 15, 2017	|<a href="documents/DokumentaciaRiadenia_02.pdf" download>Stiahnuť</a>		|		
Projektová dokumentácia	|December 15, 2017	|<a href="documents/ProjektovaDokumentacia_02.pdf" download>Stiahnuť</a>	|
Metodiky				|December 15, 2017	|<a href="documents/Metodiky_02.pdf" download>Stiahnuť</a>					|

### Letný semester - Záverečné odovzdanie

Popis					| Dátum				| 																			|
------------------------|-------------------|---------------------------------------------------------------------------|
Dokumentácia riadenia	|Máj 11, 2018	|<a href="documents/DokumentaciaRiadenia_03.pdf" download>Stiahnuť</a>			|		
Projektová dokumentácia	|Máj 11, 2018	|<a href="documents/ProjektovaDokumentacia_03.pdf" download>Stiahnuť</a>		|
Metodiky				|Máj 11, 2018	|<a href="documents/Metodiky_03.pdf" download>Stiahnuť</a>						|
Retrospektívy			|Máj 11, 2018	|<a href="documents/retrospektivy.pdf" download>Stiahnuť</a>					|
Použivateľská príručka	|Máj 11, 2018	|<a href="documents/userGuide.pdf" download>Stiahnuť</a>						|
Inštalačná príručka	|Máj 11, 2018	|<a href="documents/instalationGuide.pdf" download>Stiahnuť</a>						|

