# neverest

[![pipeline status](https://gitlab.com/neverest/neverest.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/neverest/neverest.gitlab.io/commits/master)

FIIT STU **neverest** team page built with [hugo](https://gohugo.io).

## Getting started

Follow hugo installation instructions on https://gohugo.io/getting-started/installing/.
